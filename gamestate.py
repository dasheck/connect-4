__author__ = 'das heck'

from PyQt4 import QtCore, QtGui
import json
import yaml
import sys

class GamestateManager(object):

    settings = { 'application' : { 'window' : [800,600]}, 'game' : { 'mode' : 0, 'player1' : 'Player 1', 'player2' : 'Player 2', 'player1_ai' : True, 'player2_ai' : False}}


    def __init__(self, gamestates, allowed):
        self.gamestates = gamestates
        self.allowed = allowed
        self.current = gamestates[0]
        self.current.on_enter()

        for gamestate in self.gamestates:
            gamestate.manager = self

    def transist(self, name):
        if self.current in self.allowed.keys():
            result = filter(lambda x: x.name == name, self.allowed[self.current])

            if len(result) > 0:
                self.current.on_leave()

                self.current = result[0]
                self.current.on_enter()

    def load_settings(self, filename = './settings.json'):
        try:
            file = open(filename, 'r')
            self.settings = json.load(file)
            file.close()
        except Exception as e:
            print e

    def save_settings(self, filename = './settings.json'):
        settings = json.dumps(self.settings)
        file = open(filename, 'w')
        print >> file, settings
        file.close()


class Gamestate(QtGui.QWidget):
    def __init__(self, name, parent):
        super(Gamestate, self).__init__(parent)
        self.name = name
        self.manager = None
        self.parent = parent
        self.setVisible(False)

    def mousePressEvent(self, event):
        pass

    def paintEvent(self, event):
        pass

    def keyPressEvent(self, event):
        pass

    def on_initialize(self):
        pass

    def on_shutdown(self):
        pass

    def on_enter(self):
        if self.parent.centralWidget() is not None:
            self.parent.centralWidget().setParent(None)

        self.parent.setCentralWidget(self)
        self.setVisible(True)

        self.on_initialize()

    def on_leave(self):
        self.setVisible(False)
        self.on_shutdown()

    def update(self):
        pass
