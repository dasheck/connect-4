__author__ = 'das heck'

from board import Board
import random

def test_set_chip():
    board = Board(10,2)

    assert board.set_chip(1,0) == True
    assert board.chips == {(1,0) : 0}

    assert board.set_chip(3, 1) == True
    assert board.chips == {(1,0) : 0, (3,0) : 1}

    assert board.set_chip(20, 1) == False
    assert board.chips == {(1,0) : 0, (3,0) : 1}

    assert board.set_chip(3, 0) == True
    assert board.chips == {(1,0) : 0, (3,0) : 1, (3,1) : 0}

    assert board.set_chip(3,1) == False
    assert board.chips == {(1,0) : 0, (3,0) : 1, (3,1) : 0}


def test_is_board_filled():
    board = filled_board(10,10)
    assert board.is_board_filled() == True


def test_horizontal_win():
    board = horizontal_tetris_board(0)
    assert board.player_won() == 0

    board = horizontal_tetris_board(1)
    assert board.player_won() == 1


def test_vertical_win():
    board = vertical_tetris_board(0)
    assert board.player_won() == 0

    board = vertical_tetris_board(1)
    assert board.player_won() == 1


def test_left_diag_win():
    board = left_diagonal_tetris_board(0)
    assert board.player_won() == 0

    board = left_diagonal_tetris_board(1)
    assert board.player_won() == 1


def test_right_diag_win():
    board = right_diagonal_tetris_board(0)
    assert board.player_won() == 0

    board = right_diagonal_tetris_board(1)
    assert board.player_won() == 1


def test_win():
    for i in range(1000):
        board = random_win_board()
        assert board.player_won() == 0


def test_to_rows():
    board = filled_board(3,3)
    board.chips[(1,1)] = 1
    assert board.to_rows(0) == [[(0,0), (1,0), (2,0)], [(0,1), (2,1)], [(0,2), (1,2), (2,2)]]
    assert board.to_rows(1) == [[(1,1)]]


def test_to_columns():
    board = filled_board(3,3)
    board.chips[(1,1)] = 1
    assert board.to_columns(0) == [[(0,0), (0,1), (0,2)], [(1,0), (1,2)], [(2,0), (2,1), (2,2)]]
    assert board.to_columns(1) == [[(1,1)]]


def test_to_left_diags():
    board = filled_board(3,3)
    board.chips[(1,1)] = 1
    assert board.to_left_diags(0) == [[(0, 0), (2, 2)], [(0, 1), (1, 2)], [(0, 2)], [(1, 0), (2, 1)], [(2, 0)]]
    assert board.to_left_diags(1) == [[(1,1)]]


def test_to_right_diags():
    board = filled_board(3,3)
    board.chips[(1,1)] = 1

    assert board.to_right_diags(0) == [[(0, 2), (2, 0)], [(0, 1), (1, 0)], [(0, 0)], [(1, 2), (2, 1)], [(2, 2)]]
    assert board.to_right_diags(1) == [[(1,1)]]


def filled_board(width, height, color = 0):
    board = Board(width, height)

    for x in range(width):
        for y in range(height):
            board.set_chip(x, color)

    return board


def horizontal_tetris_board(color):
    board = Board(random.randint(1,10), random.randint(1,10)+4)
    column = random.randint(0, board.width-1)
    row = random.randint(0, board.height-4)

    for i in range(4):
        board.chips[(column, row+i)] = color

    return board


def vertical_tetris_board(color):
    board = Board(random.randint(1,10)+4, random.randint(1,10))
    column = random.randint(0, board.width-4)
    row = random.randint(0, board.height-1)

    for i in range(4):
        board.chips[(column+i, row)] = color

    return board


def left_diagonal_tetris_board(color):
    board = Board(random.randint(1,10)+4, random.randint(1,10)+4)
    column = random.randint(0, board.width-4)
    row = random.randint(0, board.height-4)

    for i in range(4):
        board.chips[(column+i, row+i)] = color

    return board


def right_diagonal_tetris_board(color):
    board = Board(random.randint(1,10)+4, random.randint(1,10)+4)
    column = random.randint(3, board.width-1)
    row = random.randint(0, board.height-4)

    for i in range(4):
        board.chips[(column-i, row+i)] = color

    return board


def random_win_board():
    board = Board(10,10)

    for x in range(10):
        for y in range(10):
            board.set_chip(x, random.randint(0, 1))

    c = random.randint(3, 6)
    r = random.randint(3, 6)

    switch = random.randint(0,3)

    for i in range(4):
        if switch == 0:
            board.chips[(c+i,r)] = 0
        elif switch == 1:
            board.chips[(c,r+i)] = 0
        elif switch == 2:
            board.chips[(c+i,r+i)] = 0
        elif switch == 3:
            board.chips[(c-i,r-i)] = 0

    return board