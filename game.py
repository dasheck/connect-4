__author__ = 'das heck'

import sys
from PyQt4 import QtCore, QtGui
from board import *
from intro import *
from menu import *
from main_game import *
from option import *

class Game(QtGui.QMainWindow):

    DEFAULT_FPS = 30

    def __init__(self):
        super(Game, self).__init__()

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.Update)
        self.timer.start(1000.0/self.DEFAULT_FPS)

        intro = IntroGamestate(self)
        menu = MenuGamestate(self)
        main_game = MainGameGamestate(self)
        option = OptionGamestate(self)

        self.gamestate_manager = GamestateManager([intro, menu, main_game, option], { intro : [menu], menu : [main_game, option], main_game : [menu], option : [menu]})
        self.gamestate_manager.load_settings()

        self.resize(800,600)
        self.show()

    def mousePressEvent(self, event):
        self.gamestate_manager.current.mousePressEvent(event)

    def keyPressEvent(self, event):
        self.gamestate_manager.current.keyPressEvent(event)

    def Update(self):
        self.gamestate_manager.current.update()

    def closeEvent(self, *args, **kwargs):
        self.gamestate_manager.save_settings()

def main():
    app = QtGui.QApplication([])
    #app.setStyle(QtGui.QStyleFactory.create("GTK"))
    game = Game()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()