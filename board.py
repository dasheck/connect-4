__author__ = 'das heck'

from PyQt4 import QtCore, QtGui
from itertools import groupby

class Board(object):
    def __init__(self, width, height):

        self.width = width
        self.height = height
        self.chips = {}

    def set_chip(self, column, color):
        if 0 <= column < self.width:
            chip_pos = (column, 0)

            if len(self.chips) > 0:
                set_columns = [key for key, val in self.chips.items() if key[0] == column]

                if len(set_columns) > 0:
                    chip_pos = max(set_columns, key = lambda item: item[1])
                    chip_pos = tuple(map(sum, zip(chip_pos, (0,1))))

            if chip_pos[1] < self.height:
                self.chips[chip_pos] = color
                return True

        return False

    def remove_chip(self, column, row, color):
        print "(" + str(column) + "," + str(row) + ")"

        if (column, row) in self.chips:
            if self.chips[(column, row)] == color:
                for rest in range(row, self.height):
                    if (column, rest+1) not in self.chips:
                        del self.chips[(column, rest)]
                        break

                    self.chips[(column, rest)] = self.chips[(column, rest+1)]

                return True

        return False

    def player_won(self):
        for i in range(2):
            rows = self.to_rows(i)
            for row in rows:
                indices = []
                for current_elem, next_elem in zip(row, row[1:]+[row[0]]):
                    indices.append(current_elem[0]-next_elem[0])

                if ''.join(map(str, indices)).count('-1-1-1') > 0:
                    return i

            columns = self.to_columns(i)
            for column in columns:
                indices = []
                for current_elem, next_elem in zip(column, column[1:]+[column[0]]):
                    indices.append(current_elem[1]-next_elem[1])

                if ''.join(map(str, indices)).count('-1-1-1') > 0:
                    return i

            left_diags = self.to_left_diags(i)
            for left_diag in left_diags:
                indices = []
                for current_elem, next_elem in zip(left_diag, left_diag[1:]+[left_diag[0]]):
                    indices.append((current_elem[0]-next_elem[0], current_elem[1]-next_elem[1]))

                if ''.join(map(str, indices)).count('(-1, -1)(-1, -1)(-1, -1)') > 0:
                    return i

            right_diags = self.to_right_diags(i)
            for right_diag in right_diags:
                indices = []
                for current_elem, next_elem in zip(right_diag, right_diag[1:]+[right_diag[0]]):
                    indices.append((current_elem[0]-next_elem[0], current_elem[1]-next_elem[1]))

                if ''.join(map(str, indices)).count('(-1, 1)(-1, 1)(-1, 1)') > 0:
                    return i

        return -1

    def is_board_filled(self):
        return len(self.chips) == self.width*self.height

    def to_rows(self, color):
        if len(self.chips) == 0:
            return []

        return map(sorted, [list(v) for l,v in groupby(sorted([k for k, v2 in self.chips.items() if v2 == color], key = lambda item: item[1]), lambda item: item[1])])


    def to_columns(self, color):
        if len(self.chips) == 0:
            return []

        return map(sorted, [list(v) for l,v in groupby(sorted([k for k, v2 in self.chips.items() if v2 == color], key = lambda item: item[0]), lambda item: item[0])])

    def to_left_diags(self, color):
        if len(self.chips) == 0:
            return []

        result = []
        counted = []

        for x in range(self.width):
            for y in range(self.height):
                temp = []

                if y-x not in counted:
                    counted.append(y-x)
                    for straight in range(min(self.width-x, self.height-y)):
                        chip = (x+straight, y+straight)

                        if chip in self.chips:
                            if self.chips[chip] == color:
                                temp.append(chip)

                    result.append(temp)

        return filter(None, result)

    def to_right_diags(self, color):
        if len(self.chips) == 0:
            return []

        result = []
        counted = []

        for x in range(self.width):
            for y in range(self.height-1,-1,-1):
                temp = []

                if y+x not in counted:
                    counted.append(y+x)

                    for straight in range(min(self.width-x, y+1)):
                        chip = (x+straight, y-straight)

                        if chip in self.chips:
                            if self.chips[chip] == color:
                                temp.append(chip)

                    result.append(temp)

        return filter(None, result)