__author__ = 'das heck'

from gamestate import *
from PyQt4 import QtCore, QtGui
from board import *

class MainGameGamestate(Gamestate):

    size = 64
    zoom = 2

    def __init__(self, parent):
        super(MainGameGamestate, self).__init__('main_game', parent)

        self.pattern = QtGui.QPixmap("assets/iPattern" + str(self.size) + ".png")
        self.chips = [QtGui.QPixmap("assets/iChip" + str(self.size) + "orange.png"),
                      QtGui.QPixmap("assets/iChip" + str(self.size) + "green.png")]
        self.background = QtGui.QPixmap("assets/iBackground.png")
        self.trans_window = QtGui.QPixmap("assets/iTranswindow.png")
        self.player_info = [QtGui.QPixmap("assets/bElement1.png"), QtGui.QPixmap("assets/bElement2.png")]

        self.game_mode = ['Classic game', 'Tetris mode', 'Sunshine mode']

        self.current_player = 0
        self.wins = [0,0]

        self.state = -1
        self.messages = ['', '', 'The board is filled. New game gets started in ']

        self.timer = QtCore.QTimer(self)
        self.elapsed_timer = QtCore.QElapsedTimer()

    def on_initialize(self):
        self.board = Board(7,5)
        self.messages[0] = self.manager.settings['game']['player1'] + ' has won the game. New game gets started in '
        self.messages[1] = self.manager.settings['game']['player2'] + ' has won the game. New game gets started in '
        self.state = -1

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(0,0, self.background)

        x_offset = (800 - self.board.width * self.size)/2
        y_offset = (600 - self.board.height * self.size)/2

        for chip in self.board.chips:
            painter.drawPixmap(chip[0]*self.size + x_offset, abs(chip[1]-self.board.height+1)*self.size + y_offset, self.chips[self.board.chips[chip]])

        for x in range(self.board.width):
            for y in range(self.board.height):
                painter.drawPixmap(x*self.size + x_offset, y*self.size + y_offset, self.pattern)

        painter.drawPixmap((x_offset-176)/2, 140, self.trans_window)
        painter.drawPixmap(800 - 176 - (x_offset-176)/2, 140, self.trans_window)

        painter.drawPixmap(672*self.current_player, 568, self.player_info[self.current_player])

        painter.setPen(QtCore.Qt.white)
        font = painter.font()
        font.setPixelSize(14)
        font.setBold(True)
        painter.setFont(font)

        painter.drawText((x_offset-176)/2, 160, 176, 32, QtCore.Qt.AlignCenter, self.manager.settings['game']['player1'])
        painter.drawText(800-176-(x_offset-176)/2, 160, 176, 32, QtCore.Qt.AlignCenter, self.manager.settings['game']['player2'])

        painter.drawText((x_offset-176)/2+32, 192, 176, 32, QtCore.Qt.AlignLeft, 'Wins: ' + str(self.wins[0]))
        painter.drawText(800-176-(x_offset-176)/2+32, 192, 176, 32, QtCore.Qt.AlignLeft, 'Wins: '+ str(self.wins[1]))
        #painter.drawText((x_offset-176)/2+32, 224, 176, 32, QtCore.Qt.AlignLeft, 'Loss:')
        #painter.drawText(800-176-(x_offset-176)/2+32, 224, 176, 32, QtCore.Qt.AlignLeft, 'Loss:')

        painter.drawText(0,0,800,32, QtCore.Qt.AlignCenter, self.game_mode[self.manager.settings['game']['mode']])

        if self.state > -1:
            brush = painter.brush()
            font = painter.font()

            big_font = font
            big_font.setPixelSize(20)
            painter.setFont(big_font)
            painter.setBrush(QtGui.QColor(0,0,0,128))


            painter.drawRect(0,0,800,600)
            painter.drawText(0,0,800,600, QtCore.Qt.AlignCenter, self.messages[self.state] + str((2000-self.elapsed_timer.elapsed())/1000+1) + ' seconds')

            painter.setBrush(brush)

    def keyPressEvent(self, event):
        if self.state == -1:
            if event.key() == QtCore.Qt.Key_Escape:
                if self.manager is not None:
                    self.manager.transist('menu')

    def mousePressEvent(self, event):
        if self.state == -1:
            if event.button() == QtCore.Qt.LeftButton:
                if self.board.set_chip((event.x() - (800-self.board.width*self.size)/2)//self.size, self.current_player):
                    self.current_player = (self.current_player + 1)%2
            elif event.button() == QtCore.Qt.RightButton:
                if self.board.remove_chip((event.x() - (800-self.board.width*self.size)/2)//self.size, abs((event.y() - (600-self.board.height*self.size)/2)//self.size-self.board.height+1), self.current_player):
                    self.current_player = (self.current_player + 1)%2

            self.check_winning_conditions()

    def update(self):
        self.repaint()

    def check_winning_conditions(self):
        self.state = self.board.player_won()

        if self.state == -1 and self.board.is_board_filled():
            self.state = 2

        if self.state > -1:
            self.timer.timeout.connect(self.restart)
            self.timer.start(2000)
            self.elapsed_timer.start()

    def restart(self):
        print self.state
        if -1 < self.state < 2:
            print self.wins
            self.wins[self.state] += 1

        self.on_initialize()
        self.timer.stop()