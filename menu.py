__author__ = 'das heck'

from gamestate import *
from PyQt4 import QtCore, QtGui

class MenuGamestate(Gamestate):
    def __init__(self, parent):
        super(MenuGamestate, self).__init__('menu', parent)
        self.background = QtGui.QPixmap('assets/iMainBackground.png')

        main_layout = QtGui.QHBoxLayout(self)
        main_layout.addItem(QtGui.QSpacerItem(80,40, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum))

        layout = QtGui.QVBoxLayout()

        self.start_button = self.create_button(['assets/bStart.png','assets/bStart_hover.png'], self.Start_button_clicked)
        self.option_button = self.create_button(['assets/bOptionen.png','assets/bOptionen_hover.png'], self.Option_button_clicked)
        self.quit_button = self.create_button(['assets/bBeenden.png','assets/bBeenden_hover.png'], self.Quit_button_clicked)

        layout.addSpacerItem(QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding))
        layout.addWidget(self.start_button)
        layout.addWidget(self.option_button)
        layout.addWidget(self.quit_button)
        layout.addSpacerItem(QtGui.QSpacerItem(20,40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Maximum))

        main_layout.addLayout(layout)
        main_layout.addSpacerItem(QtGui.QSpacerItem(40,40, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding))

    def create_button(self, icons, clicked_slot):
        button = QtGui.QPushButton('')
        button.connect(button, QtCore.SIGNAL('clicked()'), clicked_slot)

        button.setSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Maximum)
        button.setAutoFillBackground(False)
        button.setFlat(True)

        button.setStyleSheet(QtCore.QString.fromUtf8('QPushButton {border-image: url(' + icons[0]+ '); width: 254px; height: 64px;}'\
                                                     'QPushButton:hover {border-image: url(' + icons[1] + ');}'))

        return button

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(0, 0, self.background)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            if self.manager is not None:
                self.manager.transist('menu')
            else:
                self.close()

    def update(self):
        pass

    def Start_button_clicked(self):
        self.manager.transist('main_game')

    def Option_button_clicked(self):
        self.manager.transist('option')

    def Quit_button_clicked(self):
        self.parent.close()