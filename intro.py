__author__ = 'das heck'

from gamestate import *
from PyQt4 import QtCore, QtGui

class IntroGamestate(Gamestate):
    def __init__(self, parent):
        super(IntroGamestate, self).__init__('intro', parent)
        self.background = QtGui.QPixmap('assets/intro_rpdev.png')

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.Transist)
        self.timer.start(3000)

    def on_shutdown(self):
        self.timer.stop()

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(0, 0, self.background)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            if self.manager is not None:
                self.manager.transist('menu')

    def update(self):
        self.repaint()

    def Transist(self):
        self.timer.stop()
        self.manager.transist('menu')